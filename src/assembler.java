import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class assembler 
{	
	private String fileName = "./src/GotoTest.vm";
	private String line = null;
	private int counter=0;
	public void readIt()
	{
	 try 
	 {
         FileReader fileReader =  new FileReader(fileName);
         BufferedReader bufferedReader =  new BufferedReader(fileReader);
         
         while((line = bufferedReader.readLine()) != null) 
         {
        	 line=line.replaceAll("//.*", "");
        	 line=line.trim();
        	 if(line.matches("\\s+"))
        	 {
        		 bufferedReader.skip(line.length());
        		 line=line.trim();
        	 }
        	 
        	 if(line.equals(""))
        	 {
        		 continue;
        	 }
        	 
         	 if(line.contains("push constant"))
        	 {
         		line=line.replaceAll("[a-z]", "");
        		line=line.trim();
         		System.out.println("@"+line);
        		System.out.println("D=A");
        		System.out.println("@SP");
        		System.out.println("A=M");
        		System.out.println("M=D");
        		System.out.println("@SP");
        		System.out.println("M=M+1");
        		line = line.replaceAll("[0-100]", "");
        		line = line.trim();
        	 }
        	 
        	 if(line.contains("push local"))
        	 {
        		 line=line.replaceAll("[a-z]", "");
        		 line=line.trim();
        		 System.out.println("@LCL");
        		 System.out.println("D=M");
        		 System.out.println("@"+line);
        		 System.out.println("A=D+A");
        		 System.out.println("D=M");
				 System.out.println("@SP");      	
       		     System.out.println("A=M");
       		     System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("M=M+1");
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        	 }
        	 
        	 if(line.contains("push argument"))
        	 {
        		 line=line.replaceAll("[a-z]", "");
        		 line=line.trim();
        		 System.out.println("@ARG");
        		 System.out.println("D=M");
        		 System.out.println("@"+line);
        		 System.out.println("A=D+A");
        		 System.out.println("D=M");
				 System.out.println("@SP");      	
       	     	 System.out.println("A=M");
       		     System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("M=M+1");
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        	 }
        	 
         	 if(line.contains("push static"))
        	 {
         		line=line.replaceAll("[a-z]", "");
        		line=line.trim();
        		 System.out.println("@16");
        		 System.out.println("D=A");
        		 System.out.println("@"+line);
        		 System.out.println("A=D+A");
             	 System.out.println("D=M");
			     System.out.println("@SP");      	
        		 System.out.println("A=M");
          		 System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("M=M+1");
        		line = line.replaceAll("[0-100]", "");
        		line = line.trim();
        	 }
        	 
        	 if(line.contains("push temp"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@5");
        		 System.out.println("D=A");
        	     System.out.println("@"+line);
        		 System.out.println("A=D+A");
        		 System.out.println("D=M");
				 System.out.println("@SP");      	
       		     System.out.println("A=M");
       		     System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("M=M+1");
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        	 }
        	 
        	 if(line.contains("neg"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=-M");
        		 
        		 line = line.replaceAll("[0-100]", "");
        		 line = line.trim();
        	 }
        	 
        	 if(line.contains("not"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=!M");
        		 
        		 line = line.replaceAll("[0-100]", "");
        		 line = line.trim();
        	 }
        	 
           	 if(line.contains("and"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("M=M&D");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 
        		 line = line.replaceAll("[0-100]", "");
        		 line = line.trim();
        	 }
           	 
           	 if(line.contains("or"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("M=M|D");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 
        		 line = line.replaceAll("[0-100]", "");
        		 line = line.trim();
        	 }
        	 
        	 if(line.contains("pop local"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@LCL");
        		 System.out.println("D=M");
        		 System.out.println("@"+line);
        		 System.out.println("D=D+A");
        		 System.out.println("@13");
        		 System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 System.out.println("@13");
        		 System.out.println("A=M");
        		 System.out.println("M=D");		 
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        		 
        	 }
        	 
        	 if(line.contains("pop argument"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@ARG");
        		 System.out.println("D=M");
        		 System.out.println("@"+line);
        		 System.out.println("D=D+A");
        		 System.out.println("@13");
        		 System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 System.out.println("@13");
        		 System.out.println("A=M");
        		 System.out.println("M=D");		 
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        		 
        	 }
        	 
        	 if(line.contains("pop static"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@15");
        		 System.out.println("D=A");
        		 System.out.println("@"+line);
        		 System.out.println("D=D+A");
        		 System.out.println("@13");
        		 System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 System.out.println("@13");
        		 System.out.println("A=M");
        		 System.out.println("M=D");		 
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim(); 
        	 }
        	 
        	 if(line.contains("pop temp"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@5");
        		 System.out.println("D=A");
        		 System.out.println("@"+line);
        		 System.out.println("D=D+A");
        		 System.out.println("@13");
        		 System.out.println("M=D");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 System.out.println("@13");
        		 System.out.println("A=M");
        		 System.out.println("M=D");		 
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();
        		 
        	 }
        	 
        	 if(line.contains("add"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("M=M+D");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();      		 
        	 }
        	 
        	 if(line.contains("sub"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("M=M-D");
        		 System.out.println("@SP");
        		 System.out.println("M=M-1");
        		 line = line.replaceAll("[0-100]", "");
         		 line = line.trim();      		 
        	 }
        	 
        	 if(line.contains("gt"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("AM=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("D=M-D");
        		 System.out.println("@labelone"+counter);
        		 counter=counter+1;
        		 System.out.println("D;JGT");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=0");
        		 System.out.println("@labelone"+counter);
        		 counter=counter+1;
        		 System.out.println("0;JMP");
        		 System.out.println("(labelone"+(counter-2)+")");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=-1");
        		 System.out.println("(labelone"+(counter-1)+")");
        		 
        		 
        		 line = line.trim();
        	 }
        	 
        	 if(line.contains("lt"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("AM=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("D=M-D");
        		 System.out.println("@labeltwo"+counter);
        		 counter=counter+1;
        		 System.out.println("D;JLT");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=0");
        		 System.out.println("@labeltwo"+counter);
        		 counter=counter+1;
        		 System.out.println("0;JMP");
        		 System.out.println("(labeltwo"+(counter-2)+")");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=-1");
        		 System.out.println("(labeltwo"+(counter-1)+")");
        		 
        		 
        		 line = line.trim();
        	 }
        	 
        	 if(line.contains("eq"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("@SP");
        		 System.out.println("AM=M-1");
        		 System.out.println("D=M");
        		 System.out.println("A=A-1");
        		 System.out.println("D=M-D");
        		 System.out.println("@labelthree"+counter);
        		 counter=counter+1;
        		 System.out.println("D;JEQ");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=0");
        		 System.out.println("@labelthree"+counter);
        		 counter=counter+1;
        		 System.out.println("0;JMP");
        		 System.out.println("(labelthree"+(counter-2)+")");
        		 System.out.println("@SP");
        		 System.out.println("A=M-1");
        		 System.out.println("M=-1");
        		 System.out.println("(labelthree"+(counter-1)+")");
        		 
        		 
        		 line = line.trim();
        	 }
        	 
        	 if(line.contains("label"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 
        		 System.out.println("("+line+")");
        	 }
        	 
        	 if(line.contains("goto"))
        	 {
        		 line = line.replaceAll("goto", "");
        		 line = line.trim();
        		 
        		 System.out.println("("+line+")");
        		 System.out.println("0;JMP");
        		 
        	 }
        	 
        	 
        	 if(line.contains("if-goto"))
        	 {
        		 line = line.replaceAll("[a-z]", "");
        		 line = line.trim();
        		 System.out.println("@SP");
        		 System.out.println("D=M");
        		 System.out.println("@"+line);
        		 System.out.println("1;JMP");
        	 }
         }   
         bufferedReader.close();         
     }
     catch(FileNotFoundException ex)
	 {
         System.out.println("Unable to open file '" + fileName + "'");                
     }
     catch(IOException ex) {
         System.out.println( "Error reading file '"  + fileName + "'");                  

     }
	}

	public static void main(String args[])
	{
		assembler a=new assembler();
		a.readIt();
	}
}
